<?php
class Bobadevv_FishpigHandles_Model_Observer
{

    /**
     * Adds layout handle for fishpig post category
     *
     * @event wordpress_render_layout_before
     * @param Varien_Event_Observer $observer
     */
    public function addAttributeSetHandle(Varien_Event_Observer $observer) {
        /* Load wordpress post */
        $post = Mage::registry('wordpress_post');
        /* @var $update Mage_Core_Model_Layout_Update */
        $update = Mage::getSingleton('core/layout')->getUpdate();

        if($post){
            $categories = $post->getParentCategories();

            if (count($categories) > 0) {
                foreach ($categories as $category) {
                    $update->addHandle('wordpress_post_' . strtolower($category->getName()) . '');
            return $this;
                }
            }
        }
    }
}